//C program to reverse a sentence entered by user.
#include <stdio.h>

int main()
{
    char a[100];
    int i,count=0;
    printf("Enter a sentence: ");
    gets(a);
    for(i=0;a[i]!='\0';i++)
        count++;

    printf("Reverse sentence: ");
    for(i=count-1;i>=0;i--)
        printf("%c",a[i]);

    return 0;
}
