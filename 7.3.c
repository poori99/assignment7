//c program to add and multiply two given matrices.
#include<stdio.h>

int main()
{
   int first[100][100], second[100][100], result[100][100];
   int row1, row2, col1, col2;

   printf("Enter rows and columns for the first matrix: ");
   scanf("%d %d",&row1,&col1);

   printf("Enter rows and columns for the second matrix: ");
   scanf("%d %d",&row2,&col2);

   if((col1==row2) && (row1==row2) && (col1==col2))
   {
       //get elements for the first matrix
       printf("\nEnter elements for the first matrix: \n");
       getMatrixElements(first,row1,col1);

       //get elements for the second matrix
       printf("\nEnter elements for the second matrix: \n");
       getMatrixElements(second,row2,col2);


       //multiply two matrices
       multiplyMatrix(first,second,row1,col1,row2,col2);

       //add two matrices
       addMatrix(first,second,row1,col1,row2,col2);
   }
   else
   {
        printf("Error! Enter rows and columns again.\n");
   }
   return 0;
}

//function to get matrix elements entered by the user
void getMatrixElements(int matrix[100][100],int row,int col)
{
      for(int i=0;i<row;i++)
      {
          for(int j=0;j<col;j++)
          {
              scanf("%d",&matrix[i][j]);
          }
      }
}

//function to  multiply two matrices
void multiplyMatrix(int first[100][100], int second[100][100],int row1, int col1, int row2, int col2)
{
    int result[100][100];
    for (int i=0;i<row1;i++)
     {
         for (int j=0;j<col2;j++)
         {
             result[i][j]=0;
             for (int k=0;k<row2;k++)
             {
                  result[i][j] += first[i][k] * second[k][j];
             }
         }
     }
     printf("\nMultiplication of the Matrices:\n");
     display(result,row1,col2);
}

//function to add two matrices
void addMatrix(int first[100][100], int second[100][100],int row1, int col1, int row2, int col2)
{
    int result[100][100];
    for (int i=0;i<row1;i++)
    {
         for (int j=0;j<col2;j++)
         {
             result[i][j] = first[i][j] + second[i][j];
         }
    }
    printf("\nAddition of the Matrices:\n");
    display(result,row1,col2);
}

// function to display the matrix
void display(int result[100][100], int row, int col)
{
     for (int i=0;i<row;i++)
     {
         for (int j=0;j<col;j++)
         {
              printf("%d  ", result[i][j]);
         }
         printf("\n");
     }
}
