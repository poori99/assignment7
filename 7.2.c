//c program to find the Frequency of a given character in a given string.
#include <stdio.h>

int main()
{
    char a[100], ch;
    int count = 0;

    printf("Enter a string: ");
    fgets(a,sizeof(a),stdin);

    printf("Enter a character to find its frequency: ");
    scanf("%c", &ch);

    for (int i=0; a[i] != '\0'; ++i)
    {
        if (ch == a[i])
        {
            ++count;
        }
    }

    printf("Frequency of %c = %d", ch, count);
    return 0;
}
